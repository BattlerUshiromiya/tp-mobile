package com.example.e1678638.tpmobile_caojacky.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.e1678638.tpmobile_caojacky.CoursActivity;
import com.example.e1678638.tpmobile_caojacky.DAL.ChampCompetence;
import com.example.e1678638.tpmobile_caojacky.DAL.Cours;
import com.example.e1678638.tpmobile_caojacky.Network.Api;
import com.example.e1678638.tpmobile_caojacky.Network.ApiChampCompetence;
import com.example.e1678638.tpmobile_caojacky.Network.ApiCours;
import com.example.e1678638.tpmobile_caojacky.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditCoursFragment extends Fragment {

    private int                   idCours;
    private int                   idChampCompetences;
    private List<ChampCompetence> listChampCompetence;
    private Cours                 cours;
    private EditText              txtSigle;
    private EditText              txtTitre;
    private EditText              txtDescription;
    private EditText              txtDuree;
    private EditText              txtContenu;
    private EditText              txtPonderation;
    private EditText              txtEpreuveFinale;
    private Spinner               spinChampCompetence;
    private Button                btnEdit;
    private View                  view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idCours = getActivity().getIntent().getIntExtra("idCours", -1);
        idChampCompetences = getActivity().getIntent().getIntExtra("idChampCompetences", -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_editcours, container, false);
        getCours();
        return view;
    }

    private void getCours() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<Cours> call = ApiCours.getCours(idCours);

        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(@NonNull Call<Cours> call, @NonNull Response<Cours> response) {
                cours = response.body();
                getChampCompetence();
            }

            @Override
            public void onFailure(Call<Cours> call, Throwable t) { }
        });
    }

    private void getChampCompetence() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiChampCompetence ApiChampCompetence = retrofit.create(ApiChampCompetence.class);
        Call<List<ChampCompetence>> call = ApiChampCompetence.getListeChampCompetence(cours.getId());

        call.enqueue(new Callback<List<ChampCompetence>>() {
            @Override
            public void onResponse(Call<List<ChampCompetence>> call, Response<List<ChampCompetence>> response) {
                listChampCompetence = response.body();
                affichageDetails();
            }

            @Override
            public void onFailure(Call<List<ChampCompetence>> call, Throwable t) { }
        });
    }

    private void putCours() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Cours c = new Cours();
        c.setId(idCours);
        c.setSigle(txtSigle.getText().toString());
        c.setTitre(txtTitre.getText().toString());
        c.setDescription(txtDescription.getText().toString());
        c.setDuree(Integer.parseInt(txtDuree.getText().toString()));
        c.setContenu(txtContenu.getText().toString());
        c.setPonderation(txtPonderation.getText().toString());
        c.setEpreuveFinale(txtEpreuveFinale.getText().toString());
        ChampCompetence cc = (ChampCompetence) spinChampCompetence.getSelectedItem();
        c.setIdChampCompetences(cc.getId());

        Call<Cours> call = ApiCours.putCours(idCours, c);
        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(@NonNull Call<Cours> call, @NonNull Response<Cours> response) {
                Toast.makeText(getContext(), "Saved changes", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), CoursActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Cours> call, Throwable t) { }
        });
    }

    public void affichageDetails() {
        txtSigle = view.findViewById(R.id.txt_sigle);
        txtTitre = view.findViewById(R.id.txt_titre);
        txtDescription = view.findViewById(R.id.txt_description);
        txtDuree = view.findViewById(R.id.txt_duree);
        txtContenu = view.findViewById(R.id.txt_contenu);
        txtPonderation = view.findViewById(R.id.txt_ponderation);
        txtEpreuveFinale = view.findViewById(R.id.txt_epreuvefinale);

        txtSigle.setText(cours.getSigle());
        txtTitre.setText(cours.getTitre());
        txtDescription.setText(cours.getDescription());
        txtDuree.setText(String.valueOf(cours.getDuree()));
        txtContenu.setText(cours.getContenu());
        txtPonderation.setText(cours.getPonderation());
        txtEpreuveFinale.setText(cours.getEpreuveFinale());

        spinChampCompetence = view.findViewById(R.id.spinner_champcompetence);
        ArrayAdapter<ChampCompetence> adapterChampCompetence = new ArrayAdapter<>(getActivity().getApplicationContext(),
                                                                                  android.R.layout.simple_spinner_item,
                                                                                  listChampCompetence);
        spinChampCompetence.setAdapter(adapterChampCompetence);
        for(ChampCompetence champCompetence : listChampCompetence)
            spinChampCompetence.setSelection(listChampCompetence.indexOf(champCompetence));
        adapterChampCompetence.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        btnEdit = view.findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putCours();
            }
        });
    }
}
