package com.example.e1678638.tpmobile_caojacky;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.e1678638.tpmobile_caojacky.Fragments.EditCoursFragment;

public class EditCoursActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcours);

        FragmentManager fragManage = getSupportFragmentManager();
        Fragment fragment = fragManage.findFragmentById(R.id.fragment_edit);
        if (fragment == null) {
            fragment = new EditCoursFragment();
            fragManage.beginTransaction()
                    .add(R.id.fragment_edit, fragment)
                    .commit();
        }
    }
}
