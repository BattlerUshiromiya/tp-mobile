package com.example.e1678638.tpmobile_caojacky.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.e1678638.tpmobile_caojacky.DAL.ChampCompetence;
import com.example.e1678638.tpmobile_caojacky.DAL.Cours;
import com.example.e1678638.tpmobile_caojacky.EditCoursActivity;
import com.example.e1678638.tpmobile_caojacky.Network.Api;
import com.example.e1678638.tpmobile_caojacky.Network.ApiChampCompetence;
import com.example.e1678638.tpmobile_caojacky.Network.ApiCours;
import com.example.e1678638.tpmobile_caojacky.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailsCoursFragment extends Fragment {

    private int             idCours;
    private int             idChampCompetences;
    private ChampCompetence champCompetence;
    private Cours           cours;
    private TextView        txtSigle;
    private TextView        txtTitre;
    private TextView        txtDescription;
    private TextView        txtDuree;
    private TextView        txtContenu;
    private TextView        txtPonderation;
    private TextView        txtEpreuveFinale;
    private TextView        txtChampCompetence;
    private Button          btnEdit;
    private View            view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idCours = getActivity().getIntent().getIntExtra("idCours", -1);
        idChampCompetences = getActivity().getIntent().getIntExtra("idChampCompetences", -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detailscours, container, false);

        btnEdit = view.findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditCoursActivity.class);

                intent.putExtra("idCours", idCours);
                intent.putExtra("idChampCompetences", idChampCompetences);
                startActivity(intent);
            }
        });

        getCours();
        return view;
    }

    public void affichageDetails() {
        txtSigle = view.findViewById(R.id.txt_sigle);
        txtTitre = view.findViewById(R.id.txt_titre);
        txtDescription = view.findViewById(R.id.txt_description);
        txtDuree = view.findViewById(R.id.txt_duree);
        txtContenu = view.findViewById(R.id.txt_contenu);
        txtPonderation = view.findViewById(R.id.txt_ponderation);
        txtEpreuveFinale = view.findViewById(R.id.txt_epreuvefinale);
        txtChampCompetence = view.findViewById(R.id.txt_champcompetence);

        txtSigle.setText(cours.getSigle());
        txtTitre.setText(cours.getTitre());
        txtDescription.setText(cours.getDescription());
        txtDuree.setText(Integer.toString(cours.getDuree()));
        txtContenu.setText(cours.getContenu());
        txtPonderation.setText(cours.getPonderation());
        txtEpreuveFinale.setText(cours.getEpreuveFinale());
        txtChampCompetence.setText(champCompetence.getEnonce());
    }

    private void getCours() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<Cours> call = ApiCours.getCours(idCours);

        call.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(@NonNull Call<Cours> call, @NonNull Response<Cours> response) {
                cours = response.body();
                getChampCompetence();
            }

            @Override
            public void onFailure(Call<Cours> call, Throwable t) { }
        });
    }

    private void getChampCompetence() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiChampCompetence ApiChampCompetence = retrofit.create(ApiChampCompetence.class);
        Call<ChampCompetence> call = ApiChampCompetence.getChampCompetence(idChampCompetences);

        call.enqueue(new Callback<ChampCompetence>() {
            @Override
            public void onResponse(Call<ChampCompetence> call, Response<ChampCompetence> response) {
                champCompetence = response.body();
                affichageDetails();
            }

            @Override
            public void onFailure(Call<ChampCompetence> call, Throwable t) { }
        });
    }
}
