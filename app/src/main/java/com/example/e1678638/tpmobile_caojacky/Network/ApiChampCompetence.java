package com.example.e1678638.tpmobile_caojacky.Network;

import com.example.e1678638.tpmobile_caojacky.DAL.ChampCompetence;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiChampCompetence {
    @GET("champsCompetencesProgrammeParIdCours/{id}")
    Call<List<ChampCompetence>> getListeChampCompetence(@Path("id") int id);

    @GET("ChampsCompetences/{id}")
    Call<ChampCompetence> getChampCompetence(@Path("id") int idChampCompetences);
}
