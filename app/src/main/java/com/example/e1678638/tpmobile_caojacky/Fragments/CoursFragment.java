package com.example.e1678638.tpmobile_caojacky.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.e1678638.tpmobile_caojacky.Adapters.CoursListAdapter;
import com.example.e1678638.tpmobile_caojacky.DAL.Cours;
import com.example.e1678638.tpmobile_caojacky.DetailsCoursActivity;
import com.example.e1678638.tpmobile_caojacky.Network.Api;
import com.example.e1678638.tpmobile_caojacky.Network.ApiCours;
import com.example.e1678638.tpmobile_caojacky.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoursFragment extends Fragment {

    private ListView listView;
    private List<Cours> listCours;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getListeDeCours();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cours, container, false);

        listView = v.findViewById(R.id.cours_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), DetailsCoursActivity.class);
                int idCours = listCours.get(position).getId();
                int idChampCompetences = listCours.get(position).getIdChampCompetences();

                intent.putExtra("idCours", idCours);
                intent.putExtra("idChampCompetences", idChampCompetences);
                startActivity(intent);
            }
        });
        return v;
    }

    private void getListeDeCours() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiCours ApiCours = retrofit.create(ApiCours.class);

        Call<List<Cours>> call = ApiCours.getListeCours();

        call.enqueue(new Callback<List<Cours>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cours>> call, @NonNull Response<List<Cours>> response) {
                listCours = response.body();
                affichageCours(listCours);
            }

            @Override
            public void onFailure(Call<List<Cours>> call, Throwable t) { }
        });

    }

    private void affichageCours(List<Cours> cours){
        listCours = cours;
        CoursListAdapter adapter = new CoursListAdapter(getActivity(), R.layout.activity_listview, listCours);
        listView.setAdapter(adapter);
    }

}
