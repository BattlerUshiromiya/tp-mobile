package com.example.e1678638.tpmobile_caojacky.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.e1678638.tpmobile_caojacky.DAL.Cours;
import com.example.e1678638.tpmobile_caojacky.R;

import java.util.List;

public class CoursListAdapter extends ArrayAdapter<Cours> {

    private Context context;
    private List<Cours> cours;

    public CoursListAdapter(Context context, int resource, List<Cours> cours) {
        super(context, resource, cours);
        this.context = context;
        this.cours = cours;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listCours = convertView;

        if(listCours == null)
            listCours = LayoutInflater.from(this.context).inflate(R.layout.activity_listview,parent,false);

        Cours cour = cours.get(position);

        TextView code = listCours.findViewById(R.id.txt_sigle);
        code.setText(cour.getSigle());

        TextView enonce = listCours.findViewById(R.id.txt_titre);
        enonce.setText(cour.getTitre());

        return listCours;
    }
}
