package com.example.e1678638.tpmobile_caojacky.DAL;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class ChampCompetence {
    @SerializedName("Id")
    private int id;
    @SerializedName("Enonce")
    private String enonce;
    @SerializedName("Couleur")
    private String couleur;
    @SerializedName("IdProgramme")
    private String idProgramme;

    public int getId() {
        return id;
    }

    public String getEnonce() {
        return enonce;
    }

    public String getCouleur() {
        return couleur;
    }

    public String getIdProgramme() {
        return idProgramme;
    }

    public String toString() {
        return enonce;
    }
}
