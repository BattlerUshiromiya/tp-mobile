package com.example.e1678638.tpmobile_caojacky.DAL;

import com.google.gson.annotations.SerializedName;

public class Cours {
    @SerializedName("Id")
    private int id;
    @SerializedName("Sigle")
    private String sigle;
    @SerializedName("Titre")
    private String titre;
    @SerializedName("Description")
    private String description;
    @SerializedName("Duree")
    private int duree;
    @SerializedName("Contenu")
    private String contenu;
    @SerializedName("Ponderation")
    private String ponderation;
    @SerializedName("EpreuveFinale")
    private String epreuveFinale;
    @SerializedName("IdChampCompetences")
    private int idChampCompetences;

    public int getId() { return id; }

    public String getSigle() {
        return sigle;
    }

    public String getTitre() {
        return titre;
    }

    public String getDescription() {
        return description;
    }

    public int getDuree() {
        return duree;
    }

    public String getContenu() {
        return contenu;
    }

    public String getPonderation() {
        return ponderation;
    }

    public String getEpreuveFinale() {
        return epreuveFinale;
    }

    public int getIdChampCompetences() {
        return idChampCompetences;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public void setPonderation(String ponderation) {
        this.ponderation = ponderation;
    }

    public void setEpreuveFinale(String epreuveFinale) {
        this.epreuveFinale = epreuveFinale;
    }

    public void setIdChampCompetences(int idChampCompetences) {
        this.idChampCompetences = idChampCompetences;
    }

}