package com.example.e1678638.tpmobile_caojacky.Network;

import com.example.e1678638.tpmobile_caojacky.DAL.Cours;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiCours {

    @GET("Cours")
    Call<List<Cours>> getListeCours();

    @GET("CoursParId/{id}")
    Call<Cours> getCours(@Path("id") int idCours);

    @PUT("Cours/{id}")
    Call<Cours> putCours(@Path("id") int idCours, @Body Cours cours);
}
