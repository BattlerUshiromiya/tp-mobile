package com.example.e1678638.tpmobile_caojacky;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.e1678638.tpmobile_caojacky.Fragments.DetailsCoursFragment;

public class DetailsCoursActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailscours);

        FragmentManager fragManage = getSupportFragmentManager();
        Fragment fragment = fragManage.findFragmentById(R.id.fragment_details);
        if (fragment == null) {
            fragment = new DetailsCoursFragment();
            fragManage.beginTransaction()
                    .add(R.id.fragment_details, fragment)
                    .commit();
        }
    }
}
