package com.example.e1678638.tpmobile_caojacky;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.e1678638.tpmobile_caojacky.Fragments.CoursFragment;

public class CoursActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cours);

        FragmentManager fragManage = getSupportFragmentManager();
        Fragment fragment = fragManage.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = new CoursFragment();
            fragManage.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
